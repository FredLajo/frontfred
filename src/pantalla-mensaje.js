import { LitElement, html } from 'lit-element';
import { estilosfont,contenedores} from '/src/estilos-fred.js'

class PantallaMensaje extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores
    ]}
    static get properties() {
      return {
        errorID:{type:String},
        mensajes:{type:Object}
      }
    }
  constructor(){
    super();
    this.mensajes=["Saldo disponible insuficiente",
                    "Cuenta de cargo invalida",
                    "Cuenta de abono invalida",
                    "Monto no permitido",
                    "Cuenta de cargo vacia",
                    "Cuenta de cargo vacia",
                    "Cuenta de cargo vacia"];

  }
  render() {
    return html`
    <style>
    .caja{
        z-index: 100;
        content: "";
        position: fixed;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        pointer-events: auto;
        display: flex;

      }
      #mensaje{

      }
      #texto{
        width: 200px;
        background-color: #F83939;


      }

    </style>
      <div class="jc_c ai_c caja">
        <div id="mensaje" class="contV ai_c ">
        <img  src="https://www.freepngimg.com/thumb/artwork/84883-homer-emoticon-bart-area-marge-simpson.png"
            alt = "FRED PRODUCTION" width="120px" height="120px"/>
          <div id="texto" class="ta_c  br_5 p_10 white"><b>${this.mensajes[this.errorID]}</b></div>
        </div>
      </div>
    `
  }
}

customElements.define('pantalla-mensaje', PantallaMensaje)
