import { LitElement, html } from 'lit-element';
import { estilosfont,contenedores,botones } from '/src/estilos-fred.js'

class NuevaCuenta extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores,
      botones
    ]}
  render() {
    return html`
    <style>
      #crearCuentaPanel{
        background-color: white;
      }
      #logo{
        filter: grayscale(100%);
      }
    </style>
      <div id="crearCuentaPanel" class="contV w_100 ai_c p_b_10 ">
        <div class="contH w_100 jc_c ai_c">

          <img id="logo" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQQmo_ZmLM3rqDdkf2Y3jJ_GJ-bW5KAJu73eQ&usqp=CAU"
              alt = "FRED/LUIS PRODUCTION" width="130px" height="170px"/>
          <div class="contV w_40 grey s14">
            <div class="s16 blue">Hola</div>
            <div class="s16 black">Fred Lajo Garcia</div>
            </br>
            <div>Abre una cuenta con nosotros de una manera facil, solo danos una descripción y nosotros nos encargamos del resto !!!</div>
          </div>

        </div>


        <vaadin-text-field id="nombreCuenta" label="Decripción" class="w_90" ></vaadin-text-field>
        <vaadin-radio-group label="Moneda" theme="Horizontal" class="w_90">
          <vaadin-radio-button  checked>Soles</vaadin-radio-button>
          <vaadin-radio-button >Dolaress</vaadin-radio-button>
        </vaadin-radio-group>
        <div class="contH w_70 ai_c jc_se">
        <vaadin-button @click="${this.confirmar}" class="w_40 b_confirmar m_t_10">Confirmar</vaadin-button>
        <vaadin-button @click="${this.cancelar}" class="w_40 b_cancelar m_t_10">Cancelar</vaadin-button>
        </div>
      </div>

    `
  }
  confirmar(){

  }
  cancelar(){
    location.href = "#!/";
  }
}

customElements.define('nueva-cuenta', NuevaCuenta)
