import { LitElement, html } from 'lit-element';
import { estilosfont,contenedores,botones } from '/src/estilos-fred.js'

class TransferenciaPage extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores,
      botones
    ]}
    static get properties() {
      return {
        userID:{type:String},
        data: {type: Object},
        name: { type: String },
        apepat: { type: String },
        apemat: { type: String },
        cambio: {type: Number},
        cuentasCargo: {type: Object},
        cuentaCargo:{type:String},
        cuentaAbono:{type:String},
        montoCargo:{type:Number},
        montoConvertido:{type:Number},
        montoDisponible:{type:Number},
        montoAbono:{type:Number},
        namecargo: { type: String },
        nameAbono: { type: String },
        divisa: { type: String },
        divisaCargo: { type: String },
        idError: { type: String },
        tipoAbono: { type: String },
        tipoCambio:{type:Number}
      }
    }
  constructor(){
    super();
    this.userID="";
    this.data=[];
    this.cuentasCargo=[];
    this.cambio=3.3;
    this.nameCargo="";
    this.nameAbono="";
    this.divisa="PEN";
    this.divisaCargo="";
    this.cuentaCargo="";
    this.cuentaAbono="";
    this.montoCargo=0;
    this.montoDisponible=0;
    this.montoConvertido=0;
    this.montoAbono=0;
    this.idError=-1;
    this.tipoAbono="propio";
    this.tipoCambio=3.3;
  }
  render() {
    return html`
    <style>
      .bb{background-color: green}
      .aa{background-color: cyan}
    </style>
    <pantalla-mensaje id="alerta" errorID=${this.idError} class="hidden"></pantalla-mensaje>
    <div id="transferencia" class="contV w_100 ">
        <vaadin-radio-group label="Transferencia" theme="Horizontal" class="w_100">
          <vaadin-radio-button @change="${this.prop}" checked>Cuenta propias</vaadin-radio-button>
          <vaadin-radio-button @change="${this.terc}">Cuentas de terceros</vaadin-radio-button>
        </vaadin-radio-group>

        <vaadin-combo-box id="comboCargo" label="Cuenta de cargo" class="w_100" @change="${this.selectedCargo}"></vaadin-combo-box>
        <div class="w_100 contH ai_c jc_e">
            <div class="grey p_r_10">Disponible</div>
            <div class="w_30  b_b ">
              <vaadin-text-field id="disponible"  class="w_100" readonly></vaadin-text-field>
            </div>
        </div>

        <div id="propio" class="w_100">
          <vaadin-combo-box @change="${this.selectedAbono}"
            id="comboAbono"
            label="Cuenta de abono"
            class="w_100"
            >
          </vaadin-combo-box>
        </div>
        <div id="tercero" class="w_100 hidden">
          <vaadin-radio-group label="Cuenta de abono" theme="Horizontal" class="w_100">
            <vaadin-radio-button id="propiobanco" @change="${this.loc}" checked>Propio banco</vaadin-radio-button>
            <vaadin-radio-button  @change="${this.inter}">Interbancaria</vaadin-radio-button>
          </vaadin-radio-group>

          <div id="local" class="w_100 contH ai_e jc_c ">
            <div class="w_20  b_b p_r_10">
              <vaadin-text-field  id="entidad" class="w_100" readonly></vaadin-text-field>
            </div>
            <div class="w_20  b_b p_r_10">
              <vaadin-text-field id="oficina" class="w_100" ></vaadin-text-field>
            </div>
            <div class="w_30  b_b p_r_10">
              <vaadin-text-field id="cuenta" class="w_100 " ></vaadin-text-field>
            </div>
          </div>

          <div id="interbancaria" class="w_100 contH ai_e jc_c hidden">
            <div class="w_15  b_b p_r_10">
              <vaadin-text-field id="ccientidad" class="w_100" ></vaadin-text-field>
            </div>
            <div class="w_15  b_b p_r_10">
              <vaadin-text-field id="ccioficina" class="w_100" ></vaadin-text-field>
            </div>
            <div class="w_40  b_b p_r_10">
              <vaadin-text-field id="ccicuenta" class="w_100" ></vaadin-text-field>
            </div>
            <div class="w_15  b_b p_r_10">
              <vaadin-text-field id="ccichequeo" class="w_100" ></vaadin-text-field>
            </div>
          </div>
        </div>



        <div class="w_100 contH">
          <div class="w_60  b_b p_r_10">
            <vaadin-combo-box  label="Moneda" class="w_100 " value="PEN" @change="${this.selectedDivisa}"
              items='[{"label": "Soles", "value": "PEN"},{"label": "Dolares", "value": "USD"}]'>
            </vaadin-combo-box>
          </div>
          <div class="w_40 ">
          <vaadin-number-field @change="${this.setMontoCargo}" id="montoCargo" label="Monto a transferir"
            step="0.01" min="0" max="1000" has-controls class="w_100" value="0">
          </vaadin-number-field>
          </div>
        </div>


        <vaadin-text-field label="Referencia de la operacion" class="w_100"></vaadin-text-field>
        <vaadin-button @click="${this.confirmar}" class="w_100 b_confirmar m_t_10">Confirmar</vaadin-button>
    </div>
    <confirmacion-page id="confirmacion" class="hidden"
      cuentaCargo="${this.cuentaCargo}"
      cuentaAbono="${this.cuentaAbono}"
      divisa="${this.divisa}"
      montoCargo="${this.montoConvertido}"
      montoAbono="${this.montoAbono}"
      nameCargo="${this.nameCargo}"
      nameAbono="${this.nameAbono}"

    ></confirmacion-page>
    <vaadin-button id="cancelar_b" @click="${this.cancelar}" class="w_100 b_cancelar m_t_5 hidden">Cancelar</vaadin-button>
    <iron-ajax @response="${this.bringdata}" @request="${this.cargando}"
      @error="${this.error}"
       auto url = "http://localhost:3000/api.peru/v1/users/${this.userID}/accounts"
       handle-as = "json"

       >
    </iron-ajax>

    `
  }
  setMontoCargo(e){
    this.montoCargo=this.shadowRoot.getElementById("montoCargo").value;
  }
  selectedDivisa(e){
    this.divisa=e.target.value;
  }
  selectedCargo(e){
    this.data.forEach((combo)=> {
      if(combo.accountID==e.target.value){
        let moneda="S/. ";
        if(combo.divisa=="USD"){
          moneda="$/. ";
        }
        this.shadowRoot.getElementById("disponible").value=moneda+combo.balance;
        this.shadowRoot.getElementById("entidad").value=combo.cuenta.substr(0, 4);
        this.cuentaCargo=combo.cuenta;
        this.montoDisponible=combo.balance;
        this.divisaCargo=combo.divisa;
      }
    });

    this.setCuentaAbono(e.target.value);
  }
  setCuentaAbono(valor){
    let cuentasAbono=[];

    let comboBox=this.shadowRoot.getElementById("comboAbono");
    comboBox.value="";

    this.data.forEach((combo)=> {
      if(combo.accountID!=valor){
        var option = {"label":(combo.cuenta+' - '+combo.description),"value":combo.accountID}
        cuentasAbono.push(option);
      }
    });
    comboBox.items=cuentasAbono;
  }
  selectedAbono(e){
    this.data.forEach((combo)=> {
      if(combo.accountID==e.target.value){
        this.cuentaAbono=combo.cuenta;
      }
    });


  }
  bringdata(data){
    console.log(data.detail.response);
    let comboBox=this.shadowRoot.getElementById("comboCargo");
    this.data=data.detail.response;
    data.detail.response.forEach((combo)=> {
      var option = {"label":(combo.cuenta+' - '+combo.description),"value":combo.accountID}
      this.cuentasCargo.push(option);
    });
    comboBox.items=this.cuentasCargo;

  }
  inter(){
    this.shadowRoot.getElementById("interbancaria").classList.remove('hidden');
    this.shadowRoot.getElementById("local").classList.add('hidden');
    this.cuentaAbono="";
  }
  loc(){
    this.shadowRoot.getElementById("interbancaria").classList.add('hidden');
    this.shadowRoot.getElementById("local").classList.remove('hidden');
    this.cuentaAbono="";
  }
  prop(){
    this.shadowRoot.getElementById("tercero").classList.add('hidden');
    this.shadowRoot.getElementById("propio").classList.remove('hidden');
    this.tipoAbono="propio";
  }
  terc(){
    this.shadowRoot.getElementById("tercero").classList.remove('hidden');
    this.shadowRoot.getElementById("propio").classList.add('hidden');
    this.shadowRoot.getElementById("comboAbono").value="";
    this.tipoAbono="tercero";
  }
  confirmar(){
    let error = this.verificarDatos();
    if(error<0){
      this.nameCargo=this.name+" "+this.apepat+" "+this.apemat;
      this.nameAbono="kevin garcia";
      this.montoAbono=this.montoCargo;
      if(this.divisaCargo!=this.divisa){
        if(this.divisaCargo=="USD"){
          this.montoConvertido=this.montoCargo/this.tipoCambio;
        }else{
          this.montoConvertido=this.montoCargo*this.tipoCambio;
        }
      }else{
        this.montoConvertido=this.montoCargo;
      }

      this.shadowRoot.getElementById("transferencia").classList.add('hidden');
      this.shadowRoot.getElementById("confirmacion").classList.remove('hidden');
      this.shadowRoot.getElementById("cancelar_b").classList.remove('hidden');
    }else{
      this.idError=error;
      this.shadowRoot.getElementById("alerta").classList.remove('hidden');
      setTimeout(()=>{
        this.shadowRoot.getElementById("alerta").classList.add('hidden');
      },2000);
    }
  }
  verificarDatos(){
    if(this.shadowRoot.getElementById("comboCargo").value==""){return 1;}
    if(this.tipoAbono=="propio"){
      if(this.shadowRoot.getElementById("comboAbono").value==""){return 2;}
    }else{
      if(this.shadowRoot.getElementById("propiobanco").checked){
        if(this.shadowRoot.getElementById("oficina").value.length!=4){return 2;}
        if(this.shadowRoot.getElementById("cuenta").value.length!=10){return 2;}
      }else{
        if(this.shadowRoot.getElementById("ccientidad").value.length!=3){return 2;}
        if(this.shadowRoot.getElementById("ccioficina").value.length!=3){return 2;}
        if(this.shadowRoot.getElementById("ccicuenta").value.length!=12){return 2;}
        if(this.shadowRoot.getElementById("ccichequeo").value.length!=2){return 2;}
      }
    }
    if(this.montoCargo==0){return 3;}
    if(this.divisaCargo!=this.divisa){
      if(this.divisaCargo=="USD"){
        if(this.montoCargo/this.tipoCambio>this.montoDisponible){
          return 0;
        }
      }else{
        if(this.montoCargo*this.tipoCambio>this.montoDisponible){
          return 0;
        }
      }
    }else{
      if(this.montoCargo>this.montoDisponible){
        return 0;
      }
    }

    return -1;
  }
  cancelar(){
    this.shadowRoot.getElementById("transferencia").classList.remove('hidden');
    this.shadowRoot.getElementById("confirmacion").classList.add('hidden');
    this.shadowRoot.getElementById("cancelar_b").classList.add('hidden');
  }
}

customElements.define('transferencia-page', TransferenciaPage)
