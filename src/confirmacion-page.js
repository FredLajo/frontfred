import { LitElement, html } from 'lit-element';
import { estilosfont,contenedores,botones } from '/src/estilos-fred.js'

class ConfirmacionPage extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores,
      botones
    ]}
  static get properties() {
    return {
      userID:{type:String},
      nameCargo: { type: String },
      nameAbono: { type: String },
      divisa: { type: String },
      cuentaCargo:{type:String},
      cuentaAbono:{type:String},
      montoCargo:{type:Number},
      montoAbono:{type:Number}
    }
  }
  constructor(){
    super();
    this.userID="";
    this.nameCargo="";
    this.nameAbono="";
    this.divisa="";
    this.cuentaCargo="";
    this.cuentaAbono="";
    this.montoCargo=0;
    this.montoAbono=0;
  }
  render() {
    return html`

      <div class="contV w_100">
        <div class=" contH w_100 jc_c s16 white fondo_azul p_10 br_s_5">
          <b>CONFIRMAR OPERACION</b>
        </div>
        <mensaje-data titulo="Operacion" mensaje="transferencia mismo banco"></mensaje-data>
        <mensaje-data titulo="Cuenta de Cargo" mensaje="${this.cuentaCargo}"></mensaje-data>
        <mensaje-data titulo="Titular cuenta de Cargo" mensaje="${this.nameCargo}"></mensaje-data>
        <mensaje-data titulo="Cuenta de Abono" mensaje="${this.cuentaAbono}"></mensaje-data>
        <mensaje-data titulo="Beneficiario" mensaje="${this.nameAbono}"></mensaje-data>
        <mensaje-data titulo="Importe transferido" mensaje="${this.montoAbono}"></mensaje-data>
        <mensaje-data titulo="Total cargado" mensaje="${this.montoCargo}"></mensaje-data>
        <vaadin-button class="w_100 b_enviar m_t_10">Transferir</vaadin-button>
      </div>
    `
  }

}

customElements.define('confirmacion-page', ConfirmacionPage)



class MensajeData extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores
    ]}
  static get properties() {
    return {
      titulo:{type:String},
      mensaje: { type: String }
    }
  }
  constructor(){
    super();
    this.titulo="";
    this.mensaje="";
  }
  render() {
    return html`
      <div class="contV w_100 p_l_20 p_t_10">
        <div class="blue">${this.titulo}</div>
        <div class="grey">${this.mensaje}</div>
      </div>
    `
  }
}

customElements.define('mensaje-data', MensajeData)
